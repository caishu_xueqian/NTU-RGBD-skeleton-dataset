# NTU-RGB+D人体骨架数据集

#### 介绍
NTU-RGB+D 是由南洋理工大学的Rose Lab 实验室提出来的人体（骨架）行为识别数据集，该数据集常见来源为[Rose Lab](https://rose1.ntu.edu.sg/dataset/actionRecognition/)和谷歌dirve，由于谷歌drive难以访问，Rose Lab需要申请审核后才能下载，为了方便相关研究，将数据集上传至此。

#### 百度网盘下载地址
skeleton+D0-30000，共2.84GB。

链接：https://pan.baidu.com/s/1v_EX-G_F_YbZEOsliv884A 

提取码：ewgj

skeleton+D30000-56800，共2.61GB。

链接：https://pan.baidu.com/s/1PrWOr5LPJiZt9-8br1zsGA 

提取码：2b7w